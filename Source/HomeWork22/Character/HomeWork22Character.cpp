// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "HomeWork22Character.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"

#include "Kismet/KismetMathLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Engine.h"

AHomeWork22Character::AHomeWork22Character()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	//Switch Weapon 
	WeaponSwitchComponent = CreateDefaultSubobject<UWeaponSwitchActor>(TEXT("WeaponSwitch"));

	if (WeaponSwitchComponent)
	{
		WeaponSwitchComponent->OnSwitchWeapon.AddDynamic(this, &AHomeWork22Character::WeaponInit);
	}

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UStaticMeshComponent>("CursorToWorld");
	CharHealthComp = CreateDefaultSubobject<UCharHealthComponent>("CharHealthComp");

	if (CharHealthComp)
	{
		CharHealthComp->OnDead.AddDynamic(this, &AHomeWork22Character::CharOnDeath);
		CharHealthComp->OnShieldChange.AddDynamic(this,&AHomeWork22Character::SpawnDamageWidget);
		CharHealthComp->OnHeathChange.AddDynamic(this, &AHomeWork22Character::SpawnDamageWidget);
	}

	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Characters/M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetMaterial(0, DecalMaterialAsset.Object);
	}
	CursorToWorld->SetRelativeRotation(FRotator(0.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void AHomeWork22Character::Tick(float DeltaSeconds)
{

	Super::Tick(DeltaSeconds);
	if (!InputBlock)
	{
		MovementTick(DeltaSeconds);

		NormVelocity = this->GetVelocity();
		NormVelocity.Normalize(0.0001);
		// Movement Logic
		if (ShiftPressed && MyCapsule->GetForwardVector().Equals(NormVelocity, 0.8) && !AltPressed && !RMBPressed)
		{
			ChangeMovementState(EMovementState::Sprint_State);
		}
		else if (AltPressed && !ShiftPressed && !RMBPressed)
		{
			ChangeMovementState(EMovementState::Walk_State);
		}
		else if (RMBPressed && !AltPressed && !ShiftPressed)
		{
			ChangeMovementState(EMovementState::Aim_State);
		}
		else if (!ShiftPressed && !AltPressed && !ShiftPressed && !RMBPressed)
		{
			ChangeMovementState(EMovementState::Run_State);
		}
		else if (RMBPressed && ShiftPressed && AltPressed)
		{
			ChangeMovementState(EMovementState::Walk_State);
		}
		else if (RMBPressed && AltPressed)
		{
			ChangeMovementState(EMovementState::Walk_State);
		}
		else if (RMBPressed && ShiftPressed)
		{
			ChangeMovementState(EMovementState::Aim_State);
		}
		else
		{
			ChangeMovementState(EMovementState::Run_State);
		}
	}


	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_GameTraceChannel1, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{

			FHitResult TraceHitResult; // ��� ��������� ����� ��� ���������� �� �����
			//��� � ����������� �������� ����� ��������� ���������� ����� �� ����� ������, ������� � ������ (LandTraceChannel1) � �������� ��������� � ��������� "TraceHitResult"
			//PC->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, TraceHitResult); // ��� ������� �� ���� ����� �����, ����� ��� � ����� DefaultEngine.ini =)
			PC->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery4, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal; // ��� �� �������� ����� � ������������, ���� �������� ����
			FRotator CursorR = UKismetMathLibrary::MakeRotFromZ(CursorFV);
			CursorToWorld->SetWorldLocation(TraceHitResult.Location); // ��� ������ ��� ������� ���������� � �����������
			CursorToWorld->SetWorldRotation(CursorR); // ��� ��� ������������ ���� �����

		}
	}
}

//������
void AHomeWork22Character::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &AHomeWork22Character::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &AHomeWork22Character::InputAxisY);

	//���
	NewInputComponent->BindAction(TEXT("LeftMouseButton"), EInputEvent::IE_Pressed, this, &AHomeWork22Character::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("LeftMouseButton"), EInputEvent::IE_Released, this, &AHomeWork22Character::InputAttackReleased);

	//�����������
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &AHomeWork22Character::TryReloadWeapon);


	//�������� ����� ����
	NewInputComponent->BindAction<DELEGATE_ShiftPressed>("LeftShiftPress", IE_Pressed, this, &AHomeWork22Character::SprintFunction, true);
	//��������� ����� ����
	NewInputComponent->BindAction<DELEGATE_ShiftPressed>("LeftShiftPress", IE_Released, this, &AHomeWork22Character::SprintFunction, false);
	//�������� ����� ����
	NewInputComponent->BindAction<DELEGATE_AltPressed>("AltWalk", IE_Pressed, this, &AHomeWork22Character::WalkFunction, true);
	//��������� ����� ����
	NewInputComponent->BindAction<DELEGATE_AltPressed>("AltWalk", IE_Released, this, &AHomeWork22Character::WalkFunction, false);
	// ��� ��������
	NewInputComponent->BindAction<DELEGATE_RMBPressed>("RightMouseAim", IE_Pressed, this, &AHomeWork22Character::AimFunction, true);
	// ��� ���������
	NewInputComponent->BindAction<DELEGATE_RMBPressed>("RightMouseAim", IE_Released, this, &AHomeWork22Character::AimFunction, false);

	NewInputComponent->BindAction<DELEGATE_OnenumberPressed>("SwitchWeapon0", IE_Pressed, this, &AHomeWork22Character::WeaponChangeCharacterForDelegate, int32(0));

	NewInputComponent->BindAction<DELEGATE_TwonumberPressed>("SwitchWeapon1", IE_Pressed, this, &AHomeWork22Character::WeaponChangeCharacterForDelegate, int32(1));

	NewInputComponent->BindAction<DELEGATE_ThreenumberPressed>("SwitchWeapon2", IE_Pressed, this, &AHomeWork22Character::WeaponChangeCharacterForDelegate, int32(2));

	NewInputComponent->BindAction<DELEGATE_FournumberPressed>("SwitchWeapon3", IE_Pressed, this, &AHomeWork22Character::WeaponChangeCharacterForDelegate, int32(3));


	NewInputComponent->BindAction("AbilityEnable", IE_Pressed, this, &AHomeWork22Character::TryAbilityEnabled);

	NewInputComponent->BindAction("DropCurrentWeapon",IE_Pressed, this, &AHomeWork22Character::DropCurrentWeaponChar);

}

void AHomeWork22Character::SprintFunction(bool BSprint_local)
{

	if (BSprint_local)
	{
		ShiftPressed = true;
	}
	if (!BSprint_local)
	{
		ShiftPressed = false;
	}
}

void AHomeWork22Character::WalkFunction(bool BWalk_local)
{
	if (BWalk_local)
	{
		AltPressed = true;
	}
	else
	{
		AltPressed = false;
	}

}

void AHomeWork22Character::AimFunction(bool BAim_local)
{
	if (BAim_local)
	{
		RMBPressed = true;
	}
	else
	{
		RMBPressed = false;
	}
}

void AHomeWork22Character::BeginPlay()
{
	Super::BeginPlay();

	//WeaponInit(InitWeaponName);
}

void AHomeWork22Character::InputAxisY(float Value)
{
	AxisY = Value;
}

void AHomeWork22Character::InputAxisX(float Value)
{
	AxisX = Value;
}

void AHomeWork22Character::MovementTick(float DeltaTime)
{
	if (bIsAlive)
	{

		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

		//�� ����� ����������
		APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

		if (myController)
		{
			//�������� �����, ���� ������� ��������, �� ������ TraceTypeQuery6 ("LandScapeCursor") 
			FHitResult HitResult;
			//myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, HitResult);
			myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery4, true, HitResult);
			CursorHitResultLocation = HitResult.Location;
			//������� ��������� �� ������
			SetActorRotation(FQuat(FRotator(0.0f, UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location).Yaw, 0.0f)));
			if (CurrentWeapon)
			{
				FVector Displacement = FVector(0);
				switch (MovementState)
				{
				case EMovementState::Aim_State:
				//	Displacement = FVector(0.0f, 0.0f, 160.0f);
					CurrentWeapon->ShouldReduceDispertion = true;
					break;
				case EMovementState::Walk_State:
				//	Displacement = FVector(0.0f, 0.0f, 140.0f);
					CurrentWeapon->ShouldReduceDispertion = true;
					break;
				case EMovementState::Run_State:
				//	Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispertion = true;
					break;
				case EMovementState::Sprint_State:
					//Displacement = FVector(0.0f, 0.0f, 160.0f);
					break;
				default:
					break;
				}


				if (HitResult.Location.Z + Displacement.Z > 310.0f)
				{
					CurrentWeapon->ShootEndLocation = HitResult.Location;
				}
				else
				{
					CurrentWeapon->ShootEndLocation = HitResult.Location + Displacement;
				}

			}
		}
	}
}

void AHomeWork22Character::CharacterUpdate()
{
	float ResSpeed = 600.0f;

	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;

	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;

	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;

	case EMovementState::Sprint_State:
		ResSpeed = MovementInfo.SprintSpeed;
		break;

	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;

	ADefaultWeapon* myWeapon = GetCurrentWeapon();

	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}

}

void AHomeWork22Character::ChangeMovementState(EMovementState NewMovementState)
{
	MovementState = NewMovementState;
	CharacterUpdate();
}

ADefaultWeapon* AHomeWork22Character::GetCurrentWeapon()
{
	return CurrentWeapon;
}


//������������� ������
void AHomeWork22Character::WeaponInit(FWeaponSlot _NewWeaponSlot)
{

	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}


	UHomeWork22GameInstance* myGI = Cast<UHomeWork22GameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{

		if (myGI->GetWeaponInfoByName(_NewWeaponSlot.ItemName, myWeaponInfo, PistolEquiped))
		{


			if (myWeaponInfo.WeaponClass)
			{
				//������� �����-������� ���� ������� � ����� ������������
				FVector SpawnLocation = FVector(0);
				// ������� �������� � ������� ��������� �� ���� ����
				FRotator SpawnRotation = FRotator(0);

				//���������, ������� �������� � ���� ��������� ��� ������ 
				FActorSpawnParameters SpawnParams;
				//����� ���:
				//��������� �������� ��� ������ ������ ������:
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				//�������� ������, ������� ������� ���������� �������, �� �� AHomeWork22PlayerController
				SpawnParams.Owner = this;
				//�������� ������, ������� ���������� ���� �� ������������� ������-������ - � ������ ������, Instigator'�� �������� AHomeWork22Character
				SpawnParams.Instigator = GetInstigator();
				//������� ������

				if (myWeaponInfo.WeaponEquipAnimMontage)
				{

					PlayAnimMontage(myWeaponInfo.WeaponEquipAnimMontage, myWeaponInfo.WeaponEquipAnimMontage->GetMaxCurrentTime() / myWeaponInfo.AnimEquipTime);
				}

				ADefaultWeapon* myWeapon = Cast<ADefaultWeapon>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				//������ �������� ���������!!!
				if (myWeapon)
				{
					//������� �������� ������ ������ ��� ����������
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					//��� �� ������� ������ � ����. GetMesh �������� ��� �������.

					myWeapon->AttachToComponent(GetMesh(), Rule, myWeaponInfo.InitSocketName);


					//��������� CurrentWeapon ����������� ������ ������
					CurrentWeapon = myWeapon;
					//myWeapon->InventoryWeaponCount = WeaponSwitchComponent->AmmoSlots[GetAmmoTypeIndex()].Count;
					myWeapon->WeaponSetting = myWeaponInfo;
					myWeapon->CurrentWeaponIdName = _NewWeaponSlot.ItemName;
					myWeapon->AdditionalWeaponInfo = _NewWeaponSlot.AdditionalInfo;
					myWeapon->DebugReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->OnWeaponReloadStart.AddDynamic(this, &AHomeWork22Character::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &AHomeWork22Character::WeaponReloadEnd);
					myWeapon->OnFire.AddDynamic(this, &AHomeWork22Character::FireMontage);
					myWeapon->SkeletalMeshWeapon->SetVisibility(false);


				}
			}
		}
		else
		{
			if (GEngine)
				GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("HomeWork22Character::WeaponInit - Weapon not found"));
		}
	}
}

void AHomeWork22Character::InputAttackPressed()
{
	if (bIsAlive)
	{
		AttackCharEvent(true);
	}
}

void AHomeWork22Character::InputAttackReleased()
{
	AttackCharEvent(false);
}

void AHomeWork22Character::AttackCharEvent(bool bIsFiring)
{
	ADefaultWeapon* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->SetWeaponStateFire(bIsFiring);
	}

}

void AHomeWork22Character::TryReloadWeapon()
{
	if (GetAmmoTypeIndex() != -1 && bIsAlive)
	{
		if (CurrentWeapon && WeaponSwitchComponent->AmmoSlots[GetAmmoTypeIndex()].Count != 0)
		{
			if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && !CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->InitReload();
			}
		}
	}

}

void AHomeWork22Character::WeaponReloadStart()
{
	WeaponReloadStart_BP();
	IsReloading = true;



	if (GetCurrentWeapon()->WeaponSetting.AnimCharReload)
	{
		PlayAnimMontage(GetCurrentWeapon()->WeaponSetting.AnimCharReload,
			GetCurrentWeapon()->WeaponSetting.AnimCharReload->GetMaxCurrentTime() / GetCurrentWeapon()->WeaponSetting.ReloadTime);
	}


}

void AHomeWork22Character::WeaponReloadEnd()
{
	IsReloading = false;
}
// �������� �� ����������� ������� � �������� ������

int AHomeWork22Character::GetAmmoTypeIndex()
{
	for (int i = 0; i < WeaponSwitchComponent->AmmoSlots.Num(); i++)
	{
		if (WeaponSwitchComponent->AmmoSlots[i].WeaponType == GetCurrentWeapon()->WeaponSetting.WeaponType)
		{
			if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(-1, 2, FColor::Red, FString::FromInt(i));
			}
			return i;
		}
	}
	return -1;
}





UHomeWork22GameInstance* AHomeWork22Character::GetCharacterGameInstance()
{
	UHomeWork22GameInstance* myGI = Cast<UHomeWork22GameInstance>(GetWorld()->GetGameInstance());
	return myGI;
}

void AHomeWork22Character::FireMontage(bool _bHaveAnim)
{
	if (_bHaveAnim)
	{
		if (GetCurrentWeapon()->WeaponSetting.HipAnimCharFire && !RMBPressed)
		{
			PlayAnimMontage(GetCurrentWeapon()->WeaponSetting.HipAnimCharFire, 1);
		}
		if (GetCurrentWeapon()->WeaponSetting.AimAnimCharFire && RMBPressed)
		{
			PlayAnimMontage(GetCurrentWeapon()->WeaponSetting.AimAnimCharFire, 1);
		}
	}

	WeaponSwitchComponent->WeaponSlots[WeaponSwitchComponent->GetCurrentIndex()].AdditionalInfo.Round = GetCurrentWeapon()->GetWeaponRound();

}

//���� ��������� 
void AHomeWork22Character::WeaponChangeCharacterForDelegate(int32 ChangeToIndex)
{
	UHomeWork22GameInstance* myGI = Cast<UHomeWork22GameInstance>(GetWorld()->GetGameInstance());

	if (myGI)
	{
		if (ChangeToIndex + 1 <= WeaponSwitchComponent->WeaponSlots.Num()
			&& ChangeToIndex != WeaponSwitchComponent->GetCurrentIndex()
			&& !WeaponSwitchComponent->WeaponSlots[ChangeToIndex].ItemName.IsNone()
			&& myGI->GetWeaponInfoByName(WeaponSwitchComponent->WeaponSlots[ChangeToIndex].ItemName, GetCurrentWeapon()->WeaponSetting, PistolEquiped))
		{
			StopAllAminChar();
			GetCurrentWeapon()->OnWeaponReloadEnd.Broadcast();

			/*if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Red, TEXT("WeaponChangeCharacter - true"));
			}
			if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Red, TEXT("WeaponSlots.Num() = ") + FString::FromInt(WeaponSwitchComponent->WeaponSlots.Num()) +
					TEXT(" ||| ChangeToIndex = ") + FString::FromInt(ChangeToIndex));
			}
			*/

			WeaponSwitchComponent->SwitchWeaponToIndex(ChangeToIndex, GetCurrentWeapon()->AdditionalWeaponInfo);
		}
		else
		{
			if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Red, TEXT("myGI - FALSE"));
			}

			if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Red, ChangeToIndex != WeaponSwitchComponent->WeaponIndexToDrop ? "True" : "false");
			}


		}

		
	}
}

bool AHomeWork22Character::WeaponChangeCharacter(int32 ChangeToIndex)
{

	UHomeWork22GameInstance* myGI = Cast<UHomeWork22GameInstance>(GetWorld()->GetGameInstance());

	if (myGI)
	{
		if (ChangeToIndex + 1 <= WeaponSwitchComponent->WeaponSlots.Num()
			/* && ChangeToIndex != WeaponSwitchComponent->GetCurrentIndex()*/
			&& !WeaponSwitchComponent->WeaponSlots[ChangeToIndex].ItemName.IsNone()
			&& myGI->GetWeaponInfoByName(WeaponSwitchComponent->WeaponSlots[ChangeToIndex].ItemName, GetCurrentWeapon()->WeaponSetting, PistolEquiped))
		{
			StopAllAminChar();

			WeaponSwitchComponent->SwitchWeaponToIndex(ChangeToIndex, GetCurrentWeapon()->AdditionalWeaponInfo);
			return true;
		}
		else
		{
			if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Purple, TEXT("After myGI - FALSE"));
			}

			if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Purple, ChangeToIndex != WeaponSwitchComponent->WeaponIndexToDrop ? "True" : "false");
			}


		}

		return false;

	}

	return false;

}

void AHomeWork22Character::StopAllAminChar()
{
	if (this->GetMesh()->GetAnimInstance())
	{
		this->GetMesh()->GetAnimInstance()->StopAllMontages(0.15f);
	}

	IsReloading = false;

	if (GetCurrentWeapon())
	{
		if (GetCurrentWeapon()->SkeletalMeshWeapon && GetCurrentWeapon()->SkeletalMeshWeapon->GetAnimInstance())
		{
			GetCurrentWeapon()->SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);
			GetCurrentWeapon()->SkeletalMeshWeapon->GetAnimInstance()->StopSlotAnimation(0.15f);
		}

		if (GetCurrentWeapon()->ReloadSound)
		{
			if (GetCurrentWeapon()->ReloadSound->IsPlaying())
			{
				GetCurrentWeapon()->ReloadSound->Deactivate();
			}

		}
	}
}




void AHomeWork22Character::WeaponReloadStart_BP_Implementation()
{

}

void AHomeWork22Character::WeaponReloadEnd_BP_Implementation()
{

}

void AHomeWork22Character::CharOnDeath()
{
	if (DeathAnimMontage.Num() > 0)
	{
		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Red, "AHomeWork22Character::CharOnDeath()");
		}

		int32 RandomMontageIndex = FMath::RandRange(0, DeathAnimMontage.Num() - 1);
		GetMesh()->GetAnimInstance()->Montage_Play(DeathAnimMontage[RandomMontageIndex]);

		float AnimTimeDeath = DeathAnimMontage[RandomMontageIndex]->GetMaxCurrentTime() - 0.3;

		GetWorld()->GetTimerManager().SetTimer(RagDollTimer, this, &AHomeWork22Character::RagDollEnable, AnimTimeDeath, false);

	}

	GetCurrentWeapon()->WeaponFiring = false;

	if (GetController())
	{
		GetController()->UnPossess();
	}
	UnPossessed();
	bIsAlive = false;
	GetCursorToWorld()->SetVisibility(false);
	CharDead_BP();
	
}

void AHomeWork22Character::CharDead_BP_Implementation()
{
	//BP
}



void AHomeWork22Character::RagDollEnable()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

		GetMesh()->SetSimulatePhysics(true);
	}
}

float AHomeWork22Character::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	//GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Orange, TEXT("AHomeWork22Character::TakeDamage"));
	if (bIsAlive)
	{
		CharHealthComp->ChangeCurrentHealth(-ActualDamage);
	}

	//�������� �� ��, ��� Character ������� ���� �� �������
	/*if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		//��������� ������ ���� �������� ������� ����
		ADefaultProjectile* myProjectile = Cast<ADefaultProjectile>(DamageCauser);
		if (myProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, myProjectile->ProjectileSetting.Effect, GetSurfaceType());
		}
	}
	*/

	//GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Blue, TEXT("AHomeWork22Character::TakeDamage ") + FString::SanitizeFloat(ActualDamage));
	return ActualDamage;
}

TArray<UStateEffect*> AHomeWork22Character::GetAllCurrentEffects()
{
	return Effects;
}

void AHomeWork22Character::RemoveEffect(UStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void AHomeWork22Character::AddEffect(UStateEffect* NewEffect)
{
	Effects.Add(NewEffect);
}

EPhysicalSurface AHomeWork22Character::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;

	if (CharHealthComp)
	{
		if (CharHealthComp->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
				if (myMaterial)
				{
					Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}

	return Result;
}

void AHomeWork22Character::TryAbilityEnabled()
{
	if (AbilityEffect)// TODO cooldown
	{
		UStateEffect* NewEffect = NewObject<UStateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			//Delay ��� ������� ���� 0
			NewEffect->InitObject(this, NAME_None);
		}
	}

}

void AHomeWork22Character::SpawnDamageWidget_Implementation(float CurrentHealth, float Damage)
{

}

void AHomeWork22Character::DropCurrentWeaponChar()
{
	if (WeaponSwitchComponent->WeaponSlots.Num()>1)
	{
		WeaponSwitchComponent->DropCurrentWeaponFromInventory(CurrentWeapon);
		WeaponChangeCharacter(0);
	}	
}

